const express = require('express');
const cors = require('cors');
const fileDb = require('./fileDb');
const products = require('./app/messages');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

fileDb.init().then(() => {
  console.log('Database file was loaded!');

  app.use('/messages', products(fileDb));

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});
