const fs = require('fs');

let data = null;

module.exports = {
  init: () => {
    return new Promise((resolve, reject) => {
      fs.readFile('./db.json', (err, result) => {
        if (err) {
          reject(err);
        } else {
          data = JSON.parse(result);
          resolve();
        }
      });
    });
  },

  getData: () => data,

  addItem: item => {
    data.push(item);

    let contents = JSON.stringify(data);

    return new Promise((resolve, reject) => {
      fs.writeFile('./db.json', contents, err => {
        if (err) {
          reject(err);
        } else {
          resolve(item);
        }
      });
    });
  }
};