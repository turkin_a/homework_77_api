const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage: storage});
const router = express.Router();

const createRouter = db => {
  router.get('/', (req, res) => {
    res.send(db.getData());
  });

  router.post('/', upload.single('image'), (req, res) => {
    if (req.message) {
      const post = req.body;
      post.id = nanoid();
      post.datetime = new Date();

      if (req.file) {
        post.image = req.file.filename;
      }

      db.addItem(post).then(result => {
        res.send(result);
      });
    } else {
      res.status(400).send({error: 'Message is empty'});
    }
  });

  return router;
};

module.exports = createRouter;